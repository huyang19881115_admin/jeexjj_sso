<#--
/****************************************************
 * Description: t_mall_role_perm的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/role/perm/save" id=tabId>
   <input type="hidden" name="id" value="${rolePerm.id}"/>
   
   <@formgroup title='role_id'>
	<input type="text" name="roleId" value="${rolePerm.roleId}" check-type="number">
   </@formgroup>
   <@formgroup title='permission_id'>
	<input type="text" name="permissionId" value="${rolePerm.permissionId}" check-type="number">
   </@formgroup>
</@input>