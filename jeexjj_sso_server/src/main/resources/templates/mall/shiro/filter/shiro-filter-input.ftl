<#--
/****************************************************
 * Description: t_mall_shiro_filter的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/shiro/filter/save" id=tabId>
   <input type="hidden" name="id" value="${shiroFilter.id}"/>
   
   <@formgroup title='name'>
	<input type="text" name="name" value="${shiroFilter.name}" >
   </@formgroup>
   <@formgroup title='perms'>
	<input type="text" name="perms" value="${shiroFilter.perms}" >
   </@formgroup>
   <@formgroup title='sort_order'>
	<input type="text" name="sortOrder" value="${shiroFilter.sortOrder}" check-type="number">
   </@formgroup>
</@input>