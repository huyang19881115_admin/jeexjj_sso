package com.xjj.sso.project.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.xjj.framework.spring.SpringBeanLoader;
import com.xjj.sso.project.entity.ProjectEntity;
import com.xjj.sso.project.service.ProjectService;



public class ProjectCache {
	private static HashMap<String,ProjectEntity> PROJECT_CACHE = null;
	
	
	private static HashMap<String,ProjectEntity> getMapCache()
	{
		if(null == PROJECT_CACHE)
		{
			PROJECT_CACHE = new HashMap<String,ProjectEntity>();
		}
		return PROJECT_CACHE;
	}
	
	
	/**
	 * 按code得到project
	 * @param projectCode
	 * @return
	 */
	public static ProjectEntity getByCode(String projectCode) {
		if(null == projectCode)
		{
			return null;
		}
		Set<String> keySet = ProjectCache.getMapCache().keySet();
		
		if(keySet.isEmpty())
		{
			initProjectCache();
		}else if(!keySet.contains(projectCode))
		{
			initProjectCache(projectCode);
		}
		
		ProjectEntity pp =   ProjectCache.getMapCache().get(projectCode);
		return pp;
	}
	
	
	/**
	 * 初始化缓存项目
	 */
	public static void initProjectCache()
	{
		ProjectService projectService = (ProjectService)SpringBeanLoader.getBean("projectServiceImpl");
		List<ProjectEntity> projectList = projectService.findAll();
		for (int i = 0; i < projectList.size(); i++) {
			 if(null != projectList.get(i) && null != projectList.get(i).getCode())
			 {
				 ProjectEntity project = projectList.get(i);
				 
				 
				 ProjectCache.getMapCache().put(project.getCode(), project);
			 }
		}
	}
	
	
	/**初始化项目，按projectCode
	 * @param projectCode
	 */
	public static void initProjectCache(String projectCode)
	{
		ProjectService projectService = (ProjectService)SpringBeanLoader.getBean("projectServiceImpl");
		ProjectEntity project = projectService.getByCode(projectCode);
		
		if(null != project)
		{
			 ProjectCache.getMapCache().put(project.getCode(), project);
		}
	}
	
}