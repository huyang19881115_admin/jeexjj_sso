package com.xjj.sso.server.dao;

import org.apache.ibatis.annotations.Param;

import com.xjj.sso.server.pojo.User;

public interface SSODao  {
	public User checkByEmail(@Param("email") String email);
	public User checkByMobile(@Param("mobile") String mobile);
	public User checkByLoginName(@Param("loginName") String loginName);
	
}
