package com.xjj.sso.server;

import com.xjj.sso.server.util.SSOConfiguration;

/**
 * 
 */
public class SSOConstants{
	public static final String HTTP_PRE = "http://";
	
	public static String SSO_VERIFICATION_MSG00="user not login";
	public static String SSO_VERIFICATION_MSG90="user signout success";
	
	public static String SSO_VERIFICATION_MSG0="user not exists";
	public static String SSO_VERIFICATION_MSG1="password error";
	public static String SSO_VERIFICATION_MSG2="param invalidate";
	public static String SSO_VERIFICATION_MSG3="projectCode not regist";
	
	public static String SSO_PATH_AUTHENTICATION=SSOConfiguration.get("sso.path.authentication");;
	public static String SSO_PATH_SIGNIN =SSOConfiguration.get("sso.path.signin");;
	public static String SSO_PATH_VALIDATETICKET=SSOConfiguration.get("sso.path.validateTicket");;
	public static String SSO_PATH_SIGNOUT=SSOConfiguration.get("sso.path.signout");;
	public static String JSONP="jsonp";
	//通知类型 已实现http和rabbitmq
	public static String SSO_NOTIFICATION_TYPE=SSOConfiguration.get("sso.notification.type");
}
