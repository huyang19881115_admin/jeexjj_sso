package com.xjj.sso.server.validation;

import com.xjj.sso.server.pojo.SSOIdentity;


public interface UserAuthService{

	/**
	 * 验证用户合法性
	 * @param identity
	 * @param password
	 * @return
	 */
	public SSOIdentity verificationUser(String identity,String password);

}
