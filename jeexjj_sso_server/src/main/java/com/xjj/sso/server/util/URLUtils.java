package com.xjj.sso.server.util;


public final class URLUtils {

	private URLUtils() {
	}
	
	public static String getUriFromUrl(String url)
	{
		int idx = url.indexOf("?");
		if(idx>0)
		{
			url = url.substring(0,idx);
		}
		
		return url;
	}
	
	
	public static String putParam2Url(String url,String key,String val)
	{
		int idx = url.indexOf("?");
		if(idx>0)
		{
			url +="&"+key+"="+val;
		}else
		{
			url +="?"+key+"="+val;
		}
		return url;
	}
	
	
	public static void main(String[] args) {
		System.out.println(getUriFromUrl("http://www.baidu.com?name=afafa"));
		System.out.println(putParam2Url("http://www.baidu.com?name=afafa","aa",""));
	}
	

}