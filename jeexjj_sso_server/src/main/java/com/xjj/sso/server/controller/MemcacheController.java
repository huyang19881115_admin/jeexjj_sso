/****************************************************
 * Description: Controller for 应用
 * Copyright:   Copyright (c) 2018
 * Company:     XJJ
 * @author      XJJ
**************************************************/

package com.xjj.sso.server.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xjj.framework.exception.ValidationException;
import com.xjj.framework.json.XjjJson;
import com.xjj.framework.utils.StringUtils;
import com.xjj.framework.web.SpringControllerSupport;
import com.xjj.sso.server.cache.MemCacheUtil;
import com.xjj.sso.server.pojo.User;


@Controller
@RequestMapping("/xsso/memcache")
public class MemcacheController extends SpringControllerSupport {
	@RequestMapping("/index")
	public String index(Model model){
		
		return getViewPath("index");
	}
	
	@RequestMapping("/delete")
	public @ResponseBody XjjJson delete(Model model){
		String key=this.getRequest().getParameter("key");
		if(StringUtils.isBlank(key))
		{
			throw new ValidationException("请输入要删除的键key");
		}
		
		
		Object userobj = MemCacheUtil.get(key);
		//如果缓存中有，直接返回
		if(null == userobj)
		{
			throw new ValidationException("缓存不存在");
		}
		
		MemCacheUtil.remove(key);
		return XjjJson.success("删除成功");
	}
	
	
	@RequestMapping("/get")
	public @ResponseBody User get(){
		String key=this.getRequest().getParameter("key");
		if(null != key && !"".equals(key))
		{
			Object userobj = MemCacheUtil.get(key);
			//如果缓存中有，直接返回
			if(null != userobj)
			{
				return (User)userobj;
			}
		}
		return null;
	}
}
