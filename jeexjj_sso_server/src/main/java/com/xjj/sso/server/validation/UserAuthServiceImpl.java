package com.xjj.sso.server.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xjj.framework.utils.StringUtils;
import com.xjj.sso.server.dao.SSODao;
import com.xjj.sso.server.pojo.SSOIdentity;
import com.xjj.sso.server.pojo.User;
import com.xjj.sso.server.util.CheckUtil;

@Service
public class UserAuthServiceImpl implements UserAuthService {

	@Autowired
	private SSODao ssoDao;
	/**
	 * 验证用户合法性
	 * @param identity
	 * @param password
	 * @return
	 */
	public SSOIdentity verificationUser(String identity,String password)
	{
		
		User user = null;
		
		//支持手机和邮箱登陆
		if(CheckUtil.isEmail(identity))
		{
			user = ssoDao.checkByEmail(identity);
		}else if(CheckUtil.isMobile(identity))
		{
			user = ssoDao.checkByMobile(identity);
		}else
		{
			user = ssoDao.checkByLoginName(identity);
		}
		
		if(null == user)
		{
			return SSOIdentity.error("user_not_exist");
		}
		
		if(StringUtils.isBlank(user.getPassword()) || !user.getPassword().equals(password))
		{
			return SSOIdentity.error("password_is_invalid");
		}
		SSOIdentity ssoId = SSOIdentity.success(user);
		return ssoId;
	}
}
