package com.xjj.sso;

import javax.servlet.annotation.WebServlet;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.xjj.sso.client.filter.SSOClientFilter;
import com.xjj.sso.client.session.SSOLogoutHttpSessionListener;


/**
 * @author jeexjj zhanghejie
 */
@Configuration
public class ApplicationConfig extends WebMvcConfigurerAdapter implements CommandLineRunner{
    
	/**
	 * 拦载器配置
	 */
    public void addInterceptors(InterceptorRegistry registry){
        
    }

    /**
     * 启动后执行
     */
    @Order(value=1)
   	public void run(String... args) throws Exception {
   	}
    
    
    /**
     * 注册过滤器，等同于非springboot项目在web.xml里配置
     * @return
     */
    @Bean
    public FilterRegistrationBean xjjFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new SSOClientFilter());//添加过滤器
        registration.addUrlPatterns("/auth/*");//设置过滤路径，/*所有路径
        registration.setOrder(1);//设置优先级
        //添加默认参数
        //registration.addInitParameter("name", "alue");
        //设置名称
        //registration.setName("SSOFilter");
        return registration;
    }

    /**
     * 配置session监听
     * @return
     */
    @Bean
	public ServletListenerRegistrationBean<SSOLogoutHttpSessionListener> ssoListenerRegistration() {
		ServletListenerRegistrationBean<SSOLogoutHttpSessionListener> ssoListener = new ServletListenerRegistrationBean<SSOLogoutHttpSessionListener>();
		ssoListener.setListener(new SSOLogoutHttpSessionListener());
		return ssoListener;
	}
    
    
   /* @ServletComponentScan
    @WebServlet(urlPatterns = "*.jsp",name = "JspServlet")
    public class JspServlet extends org.apache.jasper.servlet.JspServlet{
		private static final long serialVersionUID = 1L;
    }*/

}